var Config = require('../../config');
var Cheerio = require('cheerio');
var HttpHelper = require('../../helpers/scraper/http');
var GroupScraper = require('./group.js');

/**
 * Initializer Groups Scraper
 * @return {GroupsScraper}
 */
var GroupsScraper = function () {};

/**
 * Start scraping worker
 */
GroupsScraper.prototype.scrap = function () {
  for (var term of Config.scraper.terms) {
    var groupUrl = Config.scraper.groups_url.replace('%term%', term);
    HttpHelper.loadWebsiteBody(groupUrl, term, this.scrapGroups.bind(this));
  }
};

/**
 * Groups Scraping callback for parsing groups page
 * @param  {Error} err
 * @param  {string} websiteContent
 * @param  {string} websiteLink
 * @param  {string} term
 */
GroupsScraper.prototype.scrapGroups = function (err, websiteContent, websiteLink, term) {
  if (err) {
    throw err;
  }
  var $ = Cheerio.load(websiteContent);

  var index = 0;

  $('data gro').each(function () {
    var link = $(this);
    var href = link.attr('href');
    var groupPlanLink = HttpHelper.resolveLink(websiteLink, href);
    setTimeout(function () {
      (new GroupScraper(groupPlanLink, term)).scrap();
    }, index * Config.scraper.break_time); // don't request all pages at once
    index++;
  });
};

module.exports = GroupsScraper;
