var Cheerio = require('cheerio');
var HttpHelper = require('../../helpers/scraper/http');
var CheerioTableparser = require('cheerio-tableparser');
var ParserHelper = require('../../helpers/scraper/plan/parser');
var Plan = require('../../models/plan');
var PlanHelper = require('../../helpers/scraper/plan');

/**
 * Initializer Groups Scraper
 * @param  {string} url
 * @param {string} term
 * @return {GroupScraper}
 */
var GroupScraper = function (url, term) {
  this._url = url;
  this._term = term;
};

/**
 * Start scraping group
 */
GroupScraper.prototype.scrap = function () {
  HttpHelper.loadCP1250WebsiteBody(this._url, this.scrapGroup.bind(this));
};

/**
 * Group Scraping callback for parsing group page
 * @param  {Error}  err
 * @param  {string} websiteContent
 */
GroupScraper.prototype.scrapGroup = function (err, websiteContent) {
  if (err) {
    throw err;
  }
  console.log('Parsing group with url: ' + this._url);
  var $ = Cheerio.load(websiteContent);

  this.parsePlanDetails($);
  CheerioTableparser($);
  this._rawDataArray = $('table').parsetable(true, true, false);
  this.buildVirtualPlanList();
  this.savePlanList();
};

/**
 * Generate virtual plan list for future processing
 */
GroupScraper.prototype.buildVirtualPlanList = function () {
  this.parseSubjectsAndLecturers();
  this.parseRawPlanToBlocks();
  this.generateVirtualPlans();
};

/**
 * Parse subject and lecturers part of raw plan
 */
GroupScraper.prototype.parseSubjectsAndLecturers = function () {
  this._subjectsAndLecturers = ParserHelper.getSubjectsAndLecturers(this._rawDataArray);
};

/**
 * Parse raw plan to raw blocks
 */

GroupScraper.prototype.parseRawPlanToBlocks = function () {
  this._rawBlocks = ParserHelper.getRawBlocks(this._rawDataArray, this._years);
};

GroupScraper.prototype.generateVirtualPlans = function () {
  this._virtualPlans = [];
  for (var day of this._rawBlocks) {
    for (var block in day.blocks) {
      var virtualPlan = Plan.loadFromRawBlock(day.blocks[block], block, PlanHelper.getRealDate(day.date, this._years, this._term), this._subjectsAndLecturers, this._groupName);
      if (virtualPlan != null) {
        this._virtualPlans.push(virtualPlan);
      }
    }
  }
};

GroupScraper.prototype.savePlanList = function () {
  for (var plan of this._virtualPlans) {
    plan.save();
  }
};

/**
 * Parse group name and term from website's Cheerio
 * @param  {Cheerio} websiteCheerio Cheerio with loaded website content
 */
GroupScraper.prototype.parsePlanDetails = function ($) {
  // get years
  var $fonts = $('font');
  var yearsRegex = /(\d\d\d\d) \/ (\d\d\d\d)/g;
  var years = [];
  $fonts.each(function (i, elem) {
    var font = $(this).text();
    var m = null;
    if ((m = yearsRegex.exec(font)) !== null) {
      years = [m[1], m[2]];
      return false;
    }
  });
  this._years = years;

  var urlRegex = /y\/(.*).htm/g;
  this._groupName = urlRegex.exec(this._url)[1];
};

module.exports = GroupScraper;
