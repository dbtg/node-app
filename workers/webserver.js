var Config = require('../config');
var express = require('express');
var app = express();
var db = require('../services/database');

var WebServer = function () {};

WebServer.runWorker = function () {
  console.log('Starting webserver...');

  app.get('/list', function (req, res) {
    db.query('SELECT `name` FROM `group` WHERE 1', [], function (err, result) {
      if (err) {
        console.log(err);
        return res.json({
          'error': true,
          'message': 'Error while quering the database',
          'success': false
        });
      }
      res.json({
        'error': false,
        'success': true,
        'groups': result
      });
    });
  });

  app.get('/plan/:group', function (req, res) {
    db.query('SELECT b.block, b.date, b.type, b.extra, GROUP_CONCAT(DISTINCT CONCAT(r.name,\' \', r.building) SEPARATOR \',\') as room, GROUP_CONCAT(DISTINCT l.name SEPARATOR \',\') as `lecturers`, s.short, s.name as `subject` FROM `block` as `b` ' +
      'RIGHT JOIN `group` as g on g.id = b.group_id ' +
      'LEFT JOIN `room_block` as rb ON rb.block_id = b.id ' +
      'LEFT JOIN `room` as r ON r.id = rb.room_id ' +
      'LEFT JOIN `lecturer_block` as lb on lb.block_id = b.id ' +
      'LEFT JOIN `lecturer` as l on l.id = lb.lecturer_id ' +
      'LEFT JOIN `subject_block` as sb on sb.block_id = b.id ' +
      'LEFT JOIN `subject` as s on s.id = sb.subject_id ' +
      'WHERE g.name = ? ' +
      'GROUP BY b.id', [req.params.group], function (err, result) {
        if (err) {
          console.log(err);
          return res.json({
            'error': true,
            'message': 'Error while quering the database',
            'success': false
          });
        }
        res.json({
          'error': false,
          'success': true,
          'blocks': result
        });
      });
  });

  var server = app.listen(Config.api.port, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
  });
};

module.exports = WebServer;
