var Config = require('../config');
var GroupsScraper = require('./scraper/groups');

/**
 * Initialize Scraper obbject
 * @return {Scraper}
 */
var Scraper = function () {};

/**
 * Start scraping worker
 */
Scraper.runWorker = function () {
  console.log('Starting scraping...');
  var scraper = new Scraper();
  scraper.scrap();
  setTimeout(Scraper.runWorker, Config.scraper.timer);
};

/**
 * Start scraping
 */
Scraper.prototype.scrap = function () {
  // var GroupScraper = require('./scraper/group');
  // (new GroupScraper('http://plany.wel.wat.edu.pl/latogrupy/D5E1S1.htm', 'lato')).scrap();
  (new GroupsScraper()).scrap();
};

module.exports = Scraper;
