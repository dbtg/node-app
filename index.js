var Database = require('./services/database');
var Scraper = require('./workers/scraper');
var WebServer = require('./workers/webserver');

// Initialize database connection;
Database.createPool();

// Initialize workers
Scraper.runWorker();
WebServer.runWorker();
