# WEL Plan Scraper

Simple node app created for scraping data from WEL Plan and serve those data as rest api for other applications. Uses MySQL database to store those data.

### Requirements:
- Server with:
  - at least 256MB RAM
  - at least 1 core 1GHz+ processor
- nodeJS version 5.0+
- npm
- at least one public port free for REST API (for instance 31337)
- MySQL Server version 5.5+ or equivalent

### Installation:

```
npm install
cp config.js.sample config.js
### IMPORT DB STRUCTURES (file in from schema)
### EDIT CONFIG TO SET CORRECT SETTINGS ###
npm start
```
