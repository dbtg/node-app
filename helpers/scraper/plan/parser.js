var Cheerio = require('cheerio');
var PlanHelper = require('../plan');

var ParserHelper = {};

/**
 * Parse plan in raw data array to get an array of subject and lecturers
 * @param  {array} rawData
 * @return {array}
 */
ParserHelper.getSubjectsAndLecturers = function (rawData) {
  var result = [];
  var lastSubject;
  var rawArrayLength = rawData.length;
  var isSubject = true; // subjects are even and lecturers are odd.
  var isItListYet = false; // list of lecturers and subjects starts after a few empty columns and can have empty columns inside;
  for (var i in rawData[rawArrayLength - 1]) {
    var $firstColumn = Cheerio.load(rawData[rawArrayLength - 2][i].replace(/(<\/?[b|B][r|R]>)/ig, '\n'));
    var $secondColumn = Cheerio.load(rawData[rawArrayLength - 1][i].replace(/(<\/?[b|B][r|R]>)/ig, '\n'));
    if ($secondColumn.text().trim().length > 0 || isItListYet) {
      isItListYet = true;
      if (isSubject && $secondColumn.text().trim().length > 0) {
        lastSubject = $firstColumn.text().trim();
        result[lastSubject] = {'name': $secondColumn.text().trim(), 'lecturers': []};
      } else if ($secondColumn.text().trim().length > 0) {
        var lecturers = $secondColumn.text().trim().split('\n');
        var blockTypes = $firstColumn.text().trim().split('\n');
        for (var lecturerIndex in lecturers) {
          var typeInfo = blockTypes[lecturerIndex].split(' ');
          result[lastSubject]['lecturers'].push({'type': typeInfo[0], 'hours': typeInfo[1], 'name': lecturers[lecturerIndex]});
        }
      }
      isSubject = !isSubject;
    }
  }
  return result;
};

/**
 * Parse plan in raw data array to get array of blocks sorted by days
 * @param  {array} rawData
 * @param  {array} years for instance [2016, 2017] for 2016/2017
 * @return {array}
 */
ParserHelper.getRawBlocks = function (rawData, years) {
  var daysArray = [];
  var totalColumnsCount = rawData.length - 2; // skip last 2 columns
  // parse columns (skip first 2 columns)
  for (var col = 2; col < totalColumnsCount; col++) {
    var currentRow = 1; // start from row 2
    // parse days
    for (var day = 0; day < 7; day++) {
      var dayBlocks = {'date': rawData[col][currentRow], 'blocks': []};
      currentRow++;
      for (var block = 0; block < 6; block++) {
        dayBlocks['blocks'].push(rawData[col][currentRow]);
        currentRow++;
      }
      daysArray.push(dayBlocks);
      currentRow++;
    }
  }
  return daysArray;
};

/**
 * Parse group block and return result in an array for future processing
 * @param  {string} rawBlock
 * @return {array}
 */
ParserHelper.parseGroupBlock = function (rawBlock) {
  var block = {'rooms': []};
  var room;
  var rooms = [];

  rawBlock = rawBlock.replace(/(<\/?[b|B][r|R]>)/ig, '\n');
  rawBlock = Cheerio.load(rawBlock).text().trim().split('\n');
  if (rawBlock.length < 2) {
    return null; // this block is free
  } else if (rawBlock.length >= 3) {
    // there are two types of block notation : subject(type) or subject [new line] type
    if ((/(.*)\(/g).exec(rawBlock[1]) != null) {
      block.extra = rawBlock[0];
      block.subject = (/(.*)\(/g).exec(rawBlock[1])[1];
      block.type = (/\((.)\)?/g).exec(rawBlock[1])[1];
      if (rawBlock[2] != null) {
        rooms = rawBlock[2].split(';');
      }
    } else {
      if (rawBlock.length >= 4) {
        block.extra = rawBlock[0];
        block.subject = rawBlock[1];
        block.type = rawBlock[2];
        if (rawBlock[3] != null) {
          rooms = rawBlock[3].split(';');
        }
      } else {
        block.subject = rawBlock[0];
        block.type = rawBlock[1];
        if (rawBlock[2] != null) {
          rooms = rawBlock[2].split(';');
        }
      }
    }
    for (room of rooms) {
      room = room.trim().split(' ');
      block.rooms.push({
        'room': room[0],
        'building': room[1]
      });
    }
  } else {
    // there are two types of block notation : subject(type) or subject [new line] type
    if ((/(.*)\(/g).exec(rawBlock[0]) != null) {
      block.subject = (/(.*)\(/g).exec(rawBlock[0])[1];
      block.type = (/\((.)\)?/g).exec(rawBlock[0])[1];
      if (rawBlock[1] != null) {
        rooms = rawBlock[1].split(';');
      }
    } else {
      if (rawBlock[0] === '50%') {
        block.extra = rawBlock[0];
        block.subject = (/(.*)\(/g).exec(rawBlock[1])[1];
        block.type = (/\((.)\)?/g).exec(rawBlock[1])[1];
      } else {
        block.subject = rawBlock[0];
        block.type = rawBlock[1];
      }
      if (rawBlock[2] != null) {
        rooms = rawBlock[2].split(';');
      }
    }
    for (room of rooms) {
      room = room.trim().split(' ');
      block.rooms.push({
        'room': room[0],
        'building': room[1]
      });
    }
  }
  // trim slash
  for (var i in block.rooms) {
    if (block.rooms[i].room.endsWith('/')) {
      block.rooms[i].room = block.rooms[i].room.slice(0, -1);
    }
  }
  return block;
};

module.exports = ParserHelper;
