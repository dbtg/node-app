var Config = require('../../config');

var PlanHelper = function () {};
var blockTypes = {
  'ć': 'ćwiczenia',
  'w': 'wykład',
  'l': 'laboratoria',
  's': 'seminarium',
  'e': 'egzamin',
  'zp': 'zaliczenie poprawkowe',
  'sem.': 'seminarium',
  'z': 'zaliczenie',
  'p': 'projekt'
};

var months = {
  'I': '01',
  'II': '02',
  'III': '03',
  'IV': '04',
  'V': '05',
  'VI': '06',
  'VII': '07',
  'VIII': '08',
  'IX': '09',
  'X': '10',
  'XI': '11',
  'XII': '12'
};

/**
 * Convert short subject type to full type name
 * @param  {string} shortType
 * @return {string}
 */
PlanHelper.getRealBlockType = function (shortType) {
  if (blockTypes[shortType.toLowerCase()]) {
    return blockTypes[shortType.toLowerCase()];
  }
  return shortType;
};

/**
 * Get normal date from plan date
 * @param  {string} planDate
 * @param  {string} year
 * @param  {string} term
 * @return {string}
 */
PlanHelper.getRealDate = function (planDate, years, term) {
  var date = planDate.split('.');
  if (months[date[1]]) {
    var year;
    if (Config.scraper.terms[0] === term) {
      // it's first term, from april to december set first year and from january second year
      if (Number(months[date[1]]) > 4) {
        year = years[0];
      } else {
        year = years[1];
      }
    } else {
      // it's second term, it will be second year for sure;
      year = years[1];
    }
    return year + '-' + months[date[1]] + '-' + date[0];
  }
  // invalid date
  return planDate;
};

module.exports = PlanHelper;
