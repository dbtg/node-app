var Request = require('request');
var Url = require('url');
var Iconv = require('iconv').Iconv;

var HttpHelper = function () {};

/**
 * Get website body from http url
 * @param  {string}   url
 * @param  {function} callback
 */
HttpHelper.loadWebsiteBody = function (url, term, callback) {
  Request({
    url
  }, function (error, response, body) {
    return callback(error, body, url, term);
  });
};

/**
 * Get website body from windows-1250 by url and return result in utf-8
 * @param  {string}   url
 * @param  {function} callback
 */
HttpHelper.loadCP1250WebsiteBody = function (url, callback) {
  Request({
    url: url, encoding: 'binary'
  }, function (error, response, body) {
    if (!error) {
      body = new Buffer(body, 'binary');
      var iconv = new Iconv('CP1250', 'UTF8');
      body = iconv.convert(body).toString();
    }
    return callback(error, body, url);
  });
};

/**
 * Resolve link just like browser resolves <a href/>
 * @param  {string} oldUrl
 * @param  {string} newUrl
 * @return {string}
 */
HttpHelper.resolveLink = function (oldUrl, newUrl) {
  return Url.resolve(oldUrl, newUrl);
};

module.exports = HttpHelper;
