var mysql = require('mysql');
var config = require('../config.js');

function Database () {
  this.createPool();
}

/**
 * Initialize MySQL pool
 */
Database.createPool = function () {
  console.log('Creating database pool...');
  this._pool = mysql.createPool(config.mysql);
};

/**
 * Perform query to the database
 * @param  {string}   sql      sql query in mysql language
 * @param  {array}    options  array of replacements for ? marks in query
 * @param  {function} callback callback function
 */
Database.query = function (sql, options, callback) {
  if (typeof (options) === 'function') {
    return this._pool.getConnection(function (err, connection) {
      if (err) {
        if (callback) {
          return callback(err, null);
        } else {
          throw err;
        }
      }
      connection.query(sql, callback);
      connection.release();
    });
  } else {
    return this._pool.getConnection(function (err, connection) {
      if (err) {
        if (callback) {
          return callback(err, null);
        } else {
          throw err;
        }
      }
      connection.query(sql, options, callback);
      connection.release();
    });
  }
};

module.exports = Database;
