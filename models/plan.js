// var db = require('../services/database');
var Block = require('../models/block');
var Group = require('../models/group');
var Lecturer = require('../models/lecturer');
var Room = require('../models/room');
var Subject = require('../models/subject');

var ParserHelper = require('../helpers/scraper/plan/parser');
var PlanHelper = require('../helpers/scraper/plan');

var Plan = function () {};

Plan.loadFromRawBlock = function (rawBlock, blockNumber, date, subjectsAndLecturers, groupName) {
  var plan = new Plan();
  var parsedBlock = ParserHelper.parseGroupBlock(rawBlock);
  if (parsedBlock == null) {
    return null;
  }
  if (!subjectsAndLecturers[parsedBlock.subject]) {
    throw Error('Subject details for Block not found: ' + parsedBlock.subject);
  }

  plan._groupName = groupName;
  plan._subject = subjectsAndLecturers[parsedBlock.subject].name;
  plan._subjectShort = parsedBlock.subject;
  plan._blockNumber = blockNumber;
  plan._blockType = PlanHelper.getRealBlockType(parsedBlock.type);
  plan._blockDate = date;
  plan._blockExtra = (parsedBlock.extra ? parsedBlock.extra : null);
  plan._lecturers = [];
  for (var lecturer of subjectsAndLecturers[parsedBlock.subject].lecturers) {
    if (lecturer.type === parsedBlock.type) {
      plan._lecturers.push({'name': lecturer.name});
    }
  }
  plan._rooms = parsedBlock.rooms;
  return plan;
};

Plan.linkGroupToBlock = function (groupName, block) {
  var group = Group.getBy('name', groupName, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      if (result) {
        result.linkToBlock(block.getId());
      } else {
        group = new Group(groupName);
        group.save(function (err, response) {
          if (err) {
            console.log(err);
          } else {
            Plan.linkGroupToBlock(groupName, block);
          }
        });
      }
    }
  });
};

Plan.linkLecturerToBlock = function (lecturerName, block) {
  var lecturer = Lecturer.getBy('name', lecturerName, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      if (result) {
        result.linkToBlock(block.getId());
      } else {
        lecturer = new Lecturer(lecturerName);
        lecturer.save(function (err, response) {
          if (err) {
            console.log(err);
          } else {
            Plan.linkLecturerToBlock(lecturerName, block);
          }
        });
      }
    }
  });
};

Plan.linkSubjectToBlock = function (subjectName, short, block) {
  var subject = Subject.getBy('name', subjectName, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      if (result) {
        result.linkToBlock(block.getId());
      } else {
        subject = new Subject(subjectName, short);
        subject.save(function (err, response) {
          if (err) {
            console.log(err);
          } else {
            Plan.linkSubjectToBlock(subjectName, short, block);
          }
        });
      }
    }
  });
};

Plan.linkRoomToBlock = function (roomName, building, block) {
  var room = Room.getByNameAndBuilding(roomName, building, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      if (result) {
        result.linkToBlock(block.getId());
      } else {
        room = new Room(roomName, building);
        room.save(function (err, response) {
          if (err) {
            console.log(err);
          } else {
            Plan.linkRoomToBlock(roomName, building, block);
          }
        });
      }
    }
  });
};

Plan.prototype.save = function () {
  var plan = this;
  if (!this._groupId) {
    Group.getBy('name', plan._groupName, function (err, result) {
      if (err) {
        throw Error('Error while getting group ' + plan._groupName);
      }
      if (!result) {
        (new Group(plan._groupName)).save(function (err, result) {
          if (err) {
            throw Error('Unable to create group ' + plan._groupName);
          }
          plan._groupId = result.insertId;
          plan.save();
        });
        return;
      }
      plan._groupId = result.getId();
      plan.save();
    });
    return;
  }
  var block = new Block(this._blockNumber, this._blockType, this._blockDate, this._blockExtra, this._groupId);
  block.save(function (err, response) {
    if (err) {
      throw err;
    } else {
      block.setId(response.insertId);
      Plan.linkSubjectToBlock(plan._subject, plan._subjectShort, block);
      for (var lecturer of plan._lecturers) {
        Plan.linkLecturerToBlock(lecturer.name, block);
      }
      for (var room of plan._rooms) {
        Plan.linkRoomToBlock(room.room, room.building, block);
      }
    }
  });
};

module.exports = Plan;
