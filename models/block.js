var db = require('../services/database');

/**
 * Initialize Block object
 * @param {number} block
 * @param {string} type
 * @param {string} date
 * @param {string} extra
 * @param {number} groupId
 */

function Block (block, type, date, extra, groupId) {
  this._block = block;
  this._type = type;
  this._date = date;
  this._extra = extra;
  this._groupId = groupId;
}

/**
 * Load Block from database by Id
 * @param  {number}   id
 * @param  {function} callback
 * @return {Block}
 * */
Block.get = function (id, callback) {
  return this.getBy('id', id, callback);
};

/**
 * Load Block from databasy by specified field and value
 * @param  {string}          field
 * @param  {string|number}   value
 * @param  {function}        callback
 * @return {Block}
 */
Block.getBy = function (field, value, callback) {
  db.query('SELECT * FROM `block` WHERE ' + field + ' = ? limit 1;', [value], function (err, result, fields) {
    if (err) {
      return callback(err, null);
    } else if (result.length === 0) {
    }
    var block = new Block(result['block'], result['type'], result['date'], result['extra']);
    block.setId(result['id']);
    return callback(null, block);
  });
};

/**
 * Save Block to database
 * @param  {function} callback
 */
Block.prototype.save = function (callback) {
  db.query('INSERT INTO `block` (block, type, date, extra, group_id) VALUES(?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE type=?, date=?, extra=?, version=version+1',
  [this._block, this._type, this._date, this.extra, this._groupId, this._type, this._date, this._extra], callback);
};

/**
 * Delete Block from database
 * @param  {function} callback
 */
Block.prototype.delete = function () {
  if (this._id) {
    db.query('DELETE from `block` WHERE id = ?', [this._id]);
  }
};

/**
 * Get Id
 * @return {number}
 */
Block.prototype.getId = function () {
  return this._id;
};

/**
 * Set Id
 * @param  {number} id
 */
Block.prototype.setId = function (id) {
  this._id = id;
};

/**
 * Get block
 * @return {number}
 */
Block.prototype.getBlock = function () {
  return this._block;
};

/**
 * Set block
 * @param  {number} block
 */
Block.prototype.setBlock = function (block) {
  this._block = block;
};

/**
 * Get type
 * @return {string}
 */
Block.prototype.getType = function () {
  return this._type;
};

/**
 * Set type
 * @param  {string} type
 */
Block.prototype.setType = function (type) {
  this._type = type;
};

/**
 * Get date
 * @return {string}
 */
Block.prototype.getDate = function () {
  return this._date;
};

/**
 * Set date
 * @param  {string} date
 */
Block.prototype.setDate = function (date) {
  this._date = date;
};

/**
 * Get extra
 * @return {string}
 */
Block.prototype.getExtra = function () {
  return this._extra;
};

/**
 * Set Extra
 * @param  {string} extra
 */
Block.prototype.setExtra = function (extra) {
  this._extra = extra;
};

/**
 * Get group id
 * @return {string}
 */
Block.prototype.getGroupId = function () {
  return this._groupId;
};

/**
 * Set group id
 * @param  {string} groupId
 */
Block.prototype.setGroupId = function (groupId) {
  this._groupId = this._groupId;
};

/**
 * Return debug string representation of block
 * @return {string}
 */
Block.prototype.toString = function () {
  return 'id: ' + this.getId() + ', block: ' + this.getBlock() + ', type: ' + this.getType() +
  ', date: ' + this.getDate() + ', extra: ' + this.getExtra();
};

module.exports = Block;
