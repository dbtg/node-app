var db = require('../services/database');

/**
 * Initialize Lecturer object
 * @param {string} name
 */
function Lecturer (name) {
  this._name = name;
}

/**
 * Load Lecturer from database by Id
 * @param  {number}   id
 * @param  {function} callback
 * @return {Lecturer}
 * */
Lecturer.get = function (id, callback) {
  return this.getBy('id', id, callback);
};

/**
 * Load Lecturer from databasy by specified field and value
 * @param  {string}          field
 * @param  {string|number}   value
 * @param  {function}        callback
 * @return {Lecturer}
 */
Lecturer.getBy = function (field, value, callback) {
  db.query('SELECT * FROM `lecturer` WHERE ' + field + ' = ? limit 1;', [value], function (err, result, fields) {
    if (err) {
      return callback(err, null);
    } else if (result.length === 0) {
      return callback(err, false);
    }
    var lecturer = new Lecturer(result[0]['name']);
    lecturer.setId(result[0]['id']);
    return callback(null, lecturer);
  });
};

/**
 * Save Lecturer to database
 * @param  {function} callback
 */
Lecturer.prototype.save = function (callback) {
  db.query('INSERT IGNORE INTO `lecturer` (name) VALUES (?);',
  [this._name], callback);
};

/**
 * Delete Lecturer from database
 * @param  {function} callback
 */
Lecturer.prototype.delete = function () {
  if (this._id) {
    db.query('DELETE from `lecturer` WHERE id = ?', [this._id]);
  }
};

/**
 * Link Lecturer to Block in Database
 * @param  {number}   blockId
 * @param  {function} callback
 */
Lecturer.prototype.linkToBlock = function (blockId, callback) {
  if (this._id) {
    db.query('INSERT IGNORE INTO `lecturer_block` (lecturer_id, block_id) VALUES (?, ?);', [this._id, blockId], callback);
  } else {
    var err = new Error('Cannot link Lecturer to Block without having id');
    if (callback) {
      callback(err, null);
    } else {
      throw err;
    }
  }
};

/**
 * Get Id
 * @return {number}
 */
Lecturer.prototype.getId = function () {
  return this._id;
};

/**
 * Set Id
 * @param  {number} id
 */
Lecturer.prototype.setId = function (id) {
  this._id = id;
};

/**
 * Get name
 * @return {string}
 */
Lecturer.prototype.getName = function () {
  return this._name;
};

/**
 * Set name
 * @param  {string} name
 */
Lecturer.prototype.setName = function (name) {
  this._name = name;
};

/**
 * Return debug string representation of lecturer
 * @return {string}
 */
Lecturer.prototype.toString = function () {
  return 'id: ' + this.getId() + ', name: ' + this.getName();
};

module.exports = Lecturer;
