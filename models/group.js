var db = require('../services/database');

/**
 * Initialize Group object
 * @param {string} name
 */
function Group (name) {
  this._name = name;
}

/**
 * Load Group from database by Id
 * @param  {number}   id
 * @param  {function} callback
 * @return {Group}
 * */
Group.get = function (id, callback) {
  return this.getBy('id', id, callback);
};

/**
 * Load Group from databasy by specified field and value
 * @param  {string}          field
 * @param  {string|number}   value
 * @param  {function}        callback
 * @return {Group}
 */
Group.getBy = function (field, value, callback) {
  db.query('SELECT * FROM `group` WHERE ' + field + ' = ? limit 1;', [value], function (err, result, fields) {
    if (err) {
      return callback(err, null);
    } else if (result.length === 0) {
      return callback(err, false);
    }
    var group = new Group(result[0]['name']);
    group.setId(result[0]['id']);
    return callback(null, group);
  });
};

 /**
  * Save Group to database
  * @param  {function} callback
  */
Group.prototype.save = function (callback) {
  db.query('INSERT IGNORE INTO `group` (name) VALUES (?);',
  [this._name], callback);
};

/**
 * Delete Group from database
 * @param  {function} callback
 */
Group.prototype.delete = function () {
  if (this._id) {
    db.query('DELETE from `group` WHERE id = ?', [this._id]);
  }
};

/**
 * Get Id
 * @return {number}
 */
Group.prototype.getId = function () {
  return this._id;
};

/**
 * Set Id
 * @param  {number} id
 */
Group.prototype.setId = function (id) {
  this._id = id;
};

/**
 * Get name
 * @return {string}
 */
Group.prototype.getName = function () {
  return this._name;
};

/**
 * Set name
 * @param  {string} name
 */
Group.prototype.setName = function (name) {
  this._name = name;
};

/**
 * Return debug string representation of group
 * @return {string}
 */
Group.prototype.toString = function () {
  return 'id: ' + this.getId() + ', name: ' + this.getName();
};

module.exports = Group;
