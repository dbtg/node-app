var db = require('../services/database');

/**
 * Initialize Subject object
 * @param {string} name
 * @param {string} short
 */
function Subject (name, short) {
  this._name = name;
  this._short = short;
}

/**
 * Load Subject from database by Id
 * @param  {number}   id
 * @param  {function} callback
 * @return {Subject}
 * */
Subject.get = function (id, callback) {
  return this.getBy('id', id, callback);
};

/**
 * Load Subject from databasy by specified field and value
 * @param  {string}          field
 * @param  {string|number}   value
 * @param  {function}        callback
 * @return {Subject}
 */
Subject.getBy = function (field, value, callback) {
  db.query('SELECT * FROM `subject` WHERE ' + field + ' = ? limit 1;', [value], function (err, result, fields) {
    if (err) {
      return callback(err, null);
    } else if (result.length === 0) {
      return callback(err, false);
    }
    var subject = new Subject(result[0]['name'], result[0]['short']);
    subject.setId(result[0]['id']);
    return callback(null, subject);
  });
};

/**
 * Save Subject to database
 * @param  {function} callback
 */
Subject.prototype.save = function (callback) {
  db.query('INSERT IGNORE INTO `subject` (name, short) VALUES (?, ?);',
  [this._name, this._short], callback);
};

/**
 * Delete Subject from database
 * @param  {function} callback
 */
Subject.prototype.delete = function () {
  if (this._id) {
    db.query('DELETE from `subject` WHERE id = ?', [this._id]);
  }
};

/**
 * Link Subject to Block in Database
 * @param  {number}   blockId
 * @param  {function} callback
 */
Subject.prototype.linkToBlock = function (blockId, callback) {
  if (this._id) {
    db.query('INSERT IGNORE INTO `subject_block` (subject_id, block_id) VALUES (?, ?);', [this._id, blockId], callback);
  } else {
    var err = new Error('Cannot link Subject to Block without having id');
    if (callback) {
      callback(err, null);
    } else {
      throw err;
    }
  }
};

/**
 * Get Id
 * @return {number}
 */
Subject.prototype.getId = function () {
  return this._id;
};

/**
 * Set Id
 * @param  {number} id
 */
Subject.prototype.setId = function (id) {
  this._id = id;
};

/**
 * Get name
 * @return {string}
 */
Subject.prototype.getName = function () {
  return this._name;
};

/**
 * Set name
 * @param  {string} name
 */
Subject.prototype.setName = function (name) {
  this._name = name;
};

/**
 * Get short
 * @return {string}
 */
Subject.prototype.getShort = function () {
  return this._short;
};

/**
 * Set short
 * @param  {string} short
 */
Subject.prototype.setShort = function (short) {
  this._short = short;
};

/**
 * Return debug string representation of subject
 * @return {string}
 */
Subject.prototype.toString = function () {
  return 'id: ' + this.getId() + ', name: ' + this.getName() + ', short: ' + this.getShort();
};

module.exports = Subject;
