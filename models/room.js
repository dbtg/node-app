var db = require('../services/database');

/**
 * Initialize Room object
 * @param {string} name
 * @param {string} building
 */
function Room (name, building) {
  this._name = name;
  this._building = building;
}

/**
 * Load Room from database by Id
 * @param  {number}   id
 * @param  {function} callback
 * @return {Room}
 * */
Room.get = function (id, callback) {
  return this.getBy('id', id, callback);
};

/**
 * Load Room from database by specified field and value
 * @param  {string}          field
 * @param  {string|number}   value
 * @param  {function}        callback
 * @return {Room}
 */
Room.getBy = function (field, value, callback) {
  db.query('SELECT * FROM `room` WHERE ' + field + ' = ? limit 1;', [value], function (err, result, fields) {
    if (err) {
      return callback(err, null);
    } else if (result.length === 0) {
      return callback(err, false);
    }
    var room = new Room(result[0]['name'], result[0]['building']);
    room.setId(result[0]['id']);
    return callback(null, room);
  });
};

/**
 * Load Room from database by specified name and building
 * @param  {string}          name
 * @param  {string}          building
 * @param  {function}        callback
 * @return {Room}
 */
Room.getByNameAndBuilding = function (name, building, callback) {
  db.query('SELECT * FROM `room` WHERE `name` = ? AND `building` = ? limit 1;', [name, building], function (err, result, fields) {
    if (err) {
      return callback(err, null);
    } else if (result.length === 0) {
      return callback(err, false);
    }
    var room = new Room(result[0]['name'], result[0]['building']);
    room.setId(result[0]['id']);
    return callback(null, room);
  });
};

/**
 * Save Room to database
 * @param  {function} callback
 */
Room.prototype.save = function (callback) {
  db.query('INSERT IGNORE INTO `room` (name, building) VALUES (?, ?);',
  [this._name, this._building], callback);
};

/**
 * Delete Room from database
 * @param  {function} callback
 */
Room.prototype.delete = function () {
  if (this._id) {
    db.query('DELETE from `room` WHERE id = ?', [this._id]);
  }
};

/**
 * Link Room to Block in Database
 * @param  {number}   blockId
 * @param  {function} callback
 */
Room.prototype.linkToBlock = function (blockId, callback) {
  if (this._id) {
    db.query('INSERT IGNORE INTO `room_block` (room_id, block_id) VALUES (?, ?);', [this._id, blockId], callback);
  } else {
    var err = new Error('Cannot link Room to Block without having id');
    if (callback) {
      callback(err, null);
    } else {
      throw err;
    }
  }
};

/**
 * Get Id
 * @return {number}
 */
Room.prototype.getId = function () {
  return this._id;
};

/**
 * Set Id
 * @param  {number} id
 */
Room.prototype.setId = function (id) {
  this._id = id;
};

/**
 * Get name
 * @return {string}
 */
Room.prototype.getName = function () {
  return this._name;
};

/**
 * Set name
 * @param  {string} name
 */
Room.prototype.setName = function (name) {
  this._name = name;
};

/**
 * Get building
 * @return {string}
 */
Room.prototype.getBuilding = function () {
  return this._building;
};

/**
 * Set name
 * @param  {string} building
 */
Room.prototype.setBuilding = function (building) {
  this._building = building;
};

/**
 * Return debug string representation of room
 * @return {string}
 */
Room.prototype.toString = function () {
  return 'id: ' + this.getId() + ', name: ' + this.getName() + ', building: ' + this.getBuilding();
};

module.exports = Room;
